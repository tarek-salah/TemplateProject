/**
 * Created by tarek on 9/16/15.
 */

var validator = require('validator');

var gmtoffset = ["GMT-11", "GMT-10","GMT-9.5","GMT-9","GMT-8","GMT-7","GMT-6","GMT-5","GMT-4.5","GMT-4","GMT-3","GMT-2.5","GMT","GMT-2","GMT-1",
    "GMT+1","GMT+2","GMT+3","GMT+4","GMT+4.5","GMT+5","GMT+5.5","GMT+5.75","GMT+6","GMT+6.5","GMT+7","GMT+8","GMT+8.75","GMT+9","GMT+9.5","GMT+10",
    "GMT+10.5","GMT+11","GMT+11.5","GMT+12","GMT+12.75","GMT+13","GMT+14"];

exports.handleMongooseError = function(err){
    var ret_error = {};
    if (err.name == 'ValidationError') {
        ret_error.message = err.message;
        ret_error.details = [];
        for (field in err.errors) {
            var obj= {};
            obj[field]=err.errors[field].message
            ret_error.details.push(obj);
        }
    } else {
        ret_error.message = err.message;
    }
    return ret_error;
};

exports.checkUserBody = function (req, res, next){
    if(req.body.email && !validator.isEmail(req.body.email) ){
        res.status(400).json({message : 'invalid email'})
    }else if(req.body.first_name && !validator.isAlphanumeric(req.body.first_name)){
        res.status(400).json({message : 'invalid first name'})
    }else if(req.body.last_name && !validator.isAlphanumeric(req.body.last_name)){
        res.status(400).json({message : 'invalid last name'})
    }else{
        next();
    }
};

exports.deleteYourself = function (req, res, next){
    if(req.session.user && (req.params.id == req.session.user._id )) {
        delete req.session.user ;
        next()
    }else if(req.user && (req.params.id == req.user._id)){
        delete req.user ;
        next();
    }else{
        next();
    }
};