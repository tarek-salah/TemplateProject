/**
 * Created by tarek on 9/8/15.
 */

/**
 * Module dependencies.
 */

var crypto = require('crypto');
var auth_helpers = require('./authentication')

/**
 * Bytesize.
 */

var len = 128;

/**
 * Iterations. ~300ms
 */

var iterations = 12000;
var that = this;

/**
 * Hashes a password with optional `salt`, otherwise
 * generate a salt for `pass` and invoke `fn(err, salt, hash)`.
 *
 * @param {String} password to hash
 * @param {String} optional salt
 * @param {Function} callback
 * @api public
 */

exports.hash = function (pwd, salt, fn) {
    if (3 == arguments.length) {
        crypto.pbkdf2(pwd, salt, iterations, len, fn);
    } else {
        fn = salt;
        crypto.randomBytes(len, function(err, salt){
            if (err) return fn(err);
            salt = salt.toString('base64');
            crypto.pbkdf2(pwd, salt, iterations, len, function(err, hash){
                if (err) return fn(err);
                fn(null, salt, hash);
            });
        });
    }
};

exports.changePassword = function (body, callback){
    auth_helpers.authenticate(body.email,body.old_password, function (err, user){
        if(user)
            that.hash(body.new_password, callback);
        else
            callback({message:'Password is not correct'});
    });
};
