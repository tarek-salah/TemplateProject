/**
 * Created by tarek on 9/8/15.
 */

var User = require('../model/user');
var password_hash = require('./pass');
var jwt    = require('jsonwebtoken');

exports.authenticate = function (name, pass, fn) {
    User.findOne({email: name},
        function (err, user) {
            if (user) {
                if (err) return fn(new Error('cannot find user'));
                password_hash.hash(pass, user.salt, function (err, hash) {
                    if (err) return fn(err);
                    if (hash == user.hash) return fn(null, user);
                    fn(new Error('invalid password'));
                });
            } else {
                return fn(new Error('cannot find user'));
            }
        });
};

exports.userExist = function (req, res, next) {
    User.count({
        email: req.body.email
    }, function (err, count) {
        if (count === 0) {
            next();
        } else {
            req.session.error = "User Exist"
            res.render("signup" ,{error:'This email already exist'});
        }
    });
};

exports.repeatedEmail = function( req, res, next){
    User.count({
        email: req.body.email,
        _id : {$ne: req.params.id}
    }, function (err, count) {
        if (count === 0) {
            next();
        } else {
            req.session.error = "User Exist"
            res.status(400).json({message: 'Email already exist'});
        }
    });
};


exports.checkAPITokenAndSession = function(req, res, next){
    if(req.session.user){
        next();
    }else if(req.headers['x-access-token']){
        var token = req.headers['x-access-token'];
        jwt.verify(token, 'supersecret', function(err, decoded) {
            if (err) {
                return res.status(400).json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                req.user = decoded;
                next();
            }
        });
    }else{
        res.render("login",{error:''});
    }
};

exports.isAdminLevel= function(req, res, next){
    if((req.session.user && req.session.user.role == 'admin') ||
        (req.user && req.user.role == 'admin')){
        next();
    }else{
        res.status(401).json({
            message:'Not Authorized'
        })
    }
};

exports.isRecordMangerLevel = function(req, res, next){
    console.log(req.user);
    if((req.session.user  && (req.session.user.role == 'records_manager' || req.session.user.role == 'admin')) ||
        (req.user && (req.user.role == 'records_manager' || req.user.role == 'admin'))){
        next();
    }else{
        res.status(401).json({
            message:'Not Authorized'
        })
    }
};


exports.isAdminView = function(req, res, next){
    if(req.session.user && req.session.user.role == 'admin'){
        next();
    }else{
        res.render('error');
    }
};


exports.isRecordMangerView = function(req, res, next){
    if(req.session.user && (req.session.user.role == 'records_manager' || req.session.user.role == 'admin')){
        next();
    }else{
        res.render('error');
    }
};


exports.getOwnData = function (req, res, next){
    if((req.session.user && (req.params.id == req.session.user._id || req.session.user.role == 'admin')) ||
        (req.user && (req.params.id == req.user._id || req.user.role == 'admin'))){
        next();
    }else{
        res.status(401).json({
            message:'Not Authorized'
        })
    }
};