/**
 * Created by tarek on 9/14/15.
 */

$("#signup-form").validate({ // initialize the plugin
    rules: {
        first_name: {
            required: true,
            alphanumeric: true
        },
        last_name: {
            required: true,
            alphanumeric: true
        },
        email:{
            required: true,
            email:true
        },
        password:{
            required: true,
            minlength : 5
        },
        password_confirmation:{
            required: true,
            minlength : 5,
            equalTo : "#password"
        }
    }
});

$("#login-form").validate({ // initialize the plugin
    rules: {
        email: {
            required: true,
            email: true
        },
        password:{
            required: true,
            minlength : 5
        }
    }
});
