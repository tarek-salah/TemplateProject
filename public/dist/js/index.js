/**
 * Created by tarek on 9/10/15.
 */
oTable = $('#records_datatable').DataTable({
    responsive: true,
    "aoColumnDefs": [
        {
            'bSortable': false,
            'aTargets': [ -2,-3,-4 ]
        },
        { "bSearchable": false, "aTargets": [-1] }
    ],
    "aaSorting": [],
    "aoColumns": [
        { "sClass": "center vert-align" },
        { "sClass": "center vert-align" },
        { "sClass": "center vert-align" },
        { "sClass": "center vert-align clock-width" },
        { "sClass": "center vert-align" },
        { "sClass": "center vert-align" },
        { "sClass": "hidden" },
    ],
    "oLanguage": {
        //"sInfo": 'Showing _END_ records.',
        "sEmptyTable": "No timezones to show, please add at least one."
    },
    //"lengthMenu": [  25, 50, 75, 100 ],
    "bPaginate": false,
    "bLengthChange": false,
    "iDisplayLength": 50
});

$('#add_record').on('click', function(e){
    $('#add_record_form')[0].reset();
    $('#alert').addClass('hidden');
});

$("#add_record_form").validate({ // initialize the plugin
    rules: {
        name: {
            required: true,
            loginRegex: true
        },
        text: {
            required: true
        }
    },
    submitHandler: function (form) {
        var update = $('#update').prop('checked');
        var $form = $(form);
        if(update == false) {
            insertRecord($form)
        }else{
            updateRecord($form)
        }
    }
});


function insertRecord($form){
    $.ajax({
        url: '/user/'+ $form.find('input[name="user_id"]').val() + "/record",
        type: 'POST',
        data: $form.serialize(),
        success: function(result) {
            console.log(result);
            $('#add_time_record').modal('toggle');
            oTable.row.add( [
                result.name,
                result.text,
                result.offset,
                '<canvas class="CoolClock::60::'+ result.offset_number +':showDigital"></canvas>',
                '<div class="digital-clock" offset="'+result.offset_number+'">',
                '<a class="pointer edit_record" record_id="'+ result._id +'" data-toggle="modal" data-target="#add_time_record"><i class="fa fa-pencil-square-o fa-2x"></i></a> '+
                ' <a class="pointer delete_record" record_id="'+ result._id +'"><i class="fa fa-trash-o fa-2x"></i></a>',
                result.timezone
            ] ).draw( true );
            CoolClock.findAndCreateClocks();
            startdigitalClock();
        },
        error : function(xhr, error) {
            var error = jQuery.parseJSON(xhr.responseText);
            $('#alert_text').text(error.message);
            $('#alert').toggleClass('hidden');
        }
    });
}


function updateRecord($form){
    $.ajax({
        url: '/user/'+ $form.find('input[name="user_id"]').val() + "/record/"+ $form.find('input[name="record_id"]').val(),
        type: 'PUT',
        data: $form.serialize(),
        success: function(result) {
            console.log(result);
            $('#add_time_record').modal('toggle');
            var oTable = $('#records_datatable').dataTable();
            oTable.fnUpdate( [
                result.name,
                result.text,
                result.offset,
                '<canvas class="CoolClock::60::'+ result.offset_number +':showDigital"></canvas>',
                '<div class="digital-clock" offset="'+result.offset_number+'">',
                '<a class="pointer edit_record" record_id="'+ result._id +'" data-toggle="modal" data-target="#add_time_record"><i class="fa fa-pencil-square-o fa-2x"></i></a> '+
                ' <a class="pointer delete_record" record_id="'+ result._id +'"><i class="fa fa-trash-o fa-2x"></i></a>',
                result.timezone
            ],$form.find('input[name="index"]').val() );
            CoolClock.findAndCreateClocks();
            startdigitalClock();
        },
        error : function(xhr, error) {
            var error = jQuery.parseJSON(xhr.responseText);
            $('#alert_text').text(error.message);
            $('#alert').toggleClass('hidden');
        }
    });
}

$(document).on('change','#offset', function(e) {
    $("input[name='timezone']").val("manual");
});

$(document).on('click','.delete_record', function(e) {
    var that = $(this)
    $.ajax({
        url: '/user/'+ $('#user_id').html() +  "/record/" + $(this).attr("record_id"),
        type: 'DELETE',
        success: function(result) {
            console.log(result);
            var oTable = $('#records_datatable').dataTable();
            var target_row = that.parents('tr').get(0); // this line did the trick
            var aPos = oTable.fnGetPosition(target_row);
            oTable.fnDeleteRow(aPos);
        },
        error : function(xhr, error) {
            console.debug(xhr);
            console.debug(error);
        }
    });
});

$(document).on('click','.edit_record', function(e) {
    //get data-id attribute of the clicked element
    var record_id = $(this).attr('record_id');
    $('#update').prop('checked',true);
    var tr_element = $(this).parents('tr');
    var name = tr_element.children('td').eq(0).html().trim();
    var text = tr_element.children('td').eq(1).html().trim();
    var offset = tr_element.children('td').eq(2).html().trim();
    var timezone = tr_element.children('td').eq(6).html().trim();
    var oTable = $('#records_datatable').dataTable();
    var idx = oTable.fnGetPosition( tr_element[0] );
    //populate the textbox
    var form = $('#add_record_form');
    form.find('input[name="name"]').val(name);
    form.find('input[name="record_id"]').val(record_id);
    form.find('input[name="text"]').val( text);
    form.find('input[name="timezone"]').val( timezone);
    form.find('select[name="gmt"]').val(offset);
    form.find('input[name="index"]').val(idx);
});



$('#records_datatable').on( 'search.dt', function (e, settings, len) {
        CoolClock.findAndCreateClocks()
} );

$('#records_datatable').on( 'page.dt', function () {
    CoolClock.findAndCreateClocks();
    startdigitalClock();
} );


function startdigitalClock() {
    $('.digital-clock').each(function () {
        var that = $(this);
        var gmtOffset = $(this).attr('offset');
        var t = setInterval(function () {
            startTime(gmtOffset, that)
        }, 200);
    });
}
startdigitalClock();

function startTime(gmtOffset, element) {
    var now = new Date();
    var date = new Date(now.valueOf() + (gmtOffset * 1000 * 60 * 60));
    var h=date.getUTCHours();
    var m=date.getUTCMinutes();
    var s=date.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    h = checkTime(h);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
   //element.html(date.getUTCDate()+' '+months[date.getUTCMonth()] +"    "+ h+":"+m+":"+s);
    element.html( h+":"+m+":"+s +"<br>"+months[date.getUTCMonth()] + ' ' + date.getUTCDate());
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

$.validator.addMethod("loginRegex", function(value, element) {
    return this.optional(element) || /^[A-za-z0-9_\-\s]+$/i.test(value);
}, "Only letters, numbers, underscores, or dashes.");