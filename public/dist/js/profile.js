/**
 * Created by tarek on 9/12/15.
 */

$('#edit_user_modal').on('show.bs.modal', function(e) {
    //get data-id attribute of the clicked element
    //populate the textbox
    $(e.currentTarget).find('input[name="first_name"]').val($('#first_name').html().trim());
    $(e.currentTarget).find('input[name="last_name"]').val($('#last_name').html().trim());
    $(e.currentTarget).find('input[name="email"]').val($('#email').html().trim());
    $(e.currentTarget).find('input[name="id"]').val($('#user_id').html().trim());
    $('#old_password').val("");
    $('#new_password').val("");
    $('new_password_confirmation').val("");
});


$("#edit_user_form").validate({ // initialize the plugin
    rules: {
        first_name: {
            required: true,
            alphanumeric: true
        },
        last_name: {
            required: true,
            alphanumeric: true
        },
        email: {
            required: true,
            email: true
        },
        old_password: {
            required: function(element) {
                return $("#new_password").val().length > 0 || $("#new_password_confirmation").val().length > 0
            },
            minlength : 5
        },
        new_password: {
            required: function(element) {
                return $("#old_password").val().length > 0
            },
            minlength : 5
        },
        new_password_confirmation:{
            required: function(element) {
                return $("#old_password").val().length > 0
            },
            minlength : 5,
            equalTo : "#new_password"
        }
    },
    submitHandler: function (form) {
        var $form = $(form);
        // Use Ajax to submit form data
        $.ajax({
            url: '/user/'+ $form.find('input[name="id"]').val(),
            type: 'PUT',
            data: $form.serialize(),
            success: function(result) {
                $('#first_name').html(result.first_name);
                $('#last_name').html(result.last_name);
                $('#email').html(result.email);
                $('#edit_user_modal').modal('toggle');
            },
            error : function(xhr, error) {
                var error = jQuery.parseJSON(xhr.responseText);
                $('#alert_text').text(error.message);
                $('#alert').toggleClass('hidden');
            }
        });
    }
});
