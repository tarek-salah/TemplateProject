/**
 * Created by tarek on 9/9/15.
 */
$(document).ready(function() {


    $("#edit_user_form").validate({ // initialize the plugin
        rules: {
            first_name: {
                required: true,
                alphanumeric: true
            },
            last_name: {
                required: true,
                alphanumeric: true
            },
            email: {
                required: true,
                email: true
            }
        },
        submitHandler: function (form) {
            var $form = $(form);
            // Use Ajax to submit form data
            $.ajax({
                url: '/user/'+ $form.find('input[name="id"]').val(),
                type: 'PUT',
                data: $form.serialize(),
                success: function(result) {
                    console.log(result);
                    $('#edit_user_modal').modal('toggle');
                    var tr_element = $('#'+result._id);
                    tr_element.children('td').eq(1).html(result.first_name);
                    tr_element.children('td').eq(2).html(result.last_name);
                    tr_element.children('td').eq(3).html(result.email);
                },
                error : function(xhr, error) {
                    var error = jQuery.parseJSON(xhr.responseText);
                    $('#alert_text').text(error.message);
                    $('#alert').toggleClass('hidden');
                }
            });
        }
    });

    $('#dataTables-example').DataTable({
        responsive: true,
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1,-2 ]
            }
        ]
    });

    $('#edit_user_modal').on('show.bs.modal', function(e) {
        //get data-id attribute of the clicked element
        var tr_element = $(e.relatedTarget).parent().parent();
        var id = tr_element.children('td').eq(0).html();
        var first_name = tr_element.children('td').eq(1).html().trim();
        var last_name = tr_element.children('td').eq(2).html().trim();
        var email = tr_element.children('td').eq(3).html().trim();
        var role = tr_element.children('td').eq(4).html();
        //populate the textbox
        $(e.currentTarget).find('input[name="first_name"]').val(first_name);
        $(e.currentTarget).find('input[name="last_name"]').val(last_name);
        $(e.currentTarget).find('input[name="email"]').val(email);
        $(e.currentTarget).find('input[name="id"]').val(id);
        $('#old_password').val("");
        $('#new_password').val("");
        $('new_password_confirmation').val("");
    });

    $(document).on('click','.delete_user', function(e) {
        var that= $(this);
        $.ajax({
            url: '/user/'+ $(this).attr("user_id"),
            type: 'DELETE',
            success: function(result) {
                console.log("result");
                var oTable = $('#dataTables-example').dataTable();
                var target_row = that.parents('tr').get(0); // this line did the trick
                var aPos = oTable.fnGetPosition(target_row);
                oTable.fnDeleteRow(aPos);
            },
            error : function(xhr, error) {
                console.debug(xhr);
                console.debug(error);
            }
        });
    });

    $(".user_role").change(function(){
        var id = $(this).parents('tr').attr("id");
        var role = $(this).val();
        $('#loading_panel').toggleClass('hidden');
        $.ajax({
            url: '/user/'+ id +'/role',
            type: 'PUT',
            data:{role: role},
            success: function(result) {
                console.log("result");
                $('#loading_panel').toggleClass('hidden');
            },
            error : function(xhr, error) {
                console.debug(xhr);
            }
        });
    });

});