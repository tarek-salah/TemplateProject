/**
 * Created by tarek on 9/8/15.
 */
var express = require('express');
var auth = require('../helpers/authentication');
var router = express.Router();


router.get("/", function (req, res) {
    res.render("login", {error: null});
});

router.post("/", function (req, res) {
    auth.authenticate(req.body.email, req.body.password, function (err, user) {
        console.log(user);
        if (user) {
            user = user.toObject();
            delete user.records;
            delete user.salt;
            delete user.hash;
            req.session.user = user;
            res.redirect('/');
        } else {
            res.render('login', {error: 'Authentication failed, please check your ' + ' username and password.'});
        }
    });
});

module.exports = router;