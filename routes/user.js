var express = require('express');

var user = require('../model/user');
var auth_helpers = require('../helpers/authentication');
var validation = require('../helpers/validation');
var pass = require('../helpers/pass');

var router = express.Router();

/* GET users listing. */
router.get('/', auth_helpers.isAdminLevel, function (req, res, next) {
    user.listAll(function (err, users) {
        res.json({
            users: users,
            err: err
        });
    });
});

/* GET user with specific id */
router.get('/:id',auth_helpers.getOwnData, function (req, res, next) {
    var condition = {
        _id:req.params.id
    };
    user.findOne(condition, function(err, user){
        if (!err) {
            res.json(user);
        }else{
            res.status(400).json(validation.handleMongooseError(err));
        }
    });
});


/* Insert new user */
router.post('/', auth_helpers.isAdminLevel, validation.checkUserBody , function (req, res, next) {
    var password = req.body.password;
    pass.hash(password, function (err, salt, hash) {
        if (err) throw err;
        var user_entry = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            salt: salt,
            hash: hash,
            role: req.body.role
        };
        user.insert(user_entry, function (err, result) {
            if (!err) {
                res.json(result);
            } else {
                res.status(400).json(validation.handleMongooseError(err));
            }
        })
    });
});

/* Update exiting user*/
router.put('/:id', auth_helpers.getOwnData,auth_helpers.repeatedEmail,validation.checkUserBody, function (req, res, next) {
    var condition = {
        _id: req.params.id
    };
    delete req.body.id;
    delete req.body.role;
    var data = req.body;
    if(req.body.old_password && req.body.new_password){
        pass.changePassword(req.body, function(err,salt,hash){
            if(!err) {
                data.salt = salt;
                data.hash = hash;
                update_user(condition, data, res);
            }else{
                res.status(400).json(err);
            }
        });
    }else{
        update_user(condition,data, res);
    }
});

function update_user(condition , data, res){
    user.update(condition, data, function (err, user) {
        if (!err) {
            res.json(user);
        } else {
            res.status(400).json(validation.handleMongooseError(err));
        }
    });
}

/*make user admin*/
router.put('/:id/role',auth_helpers.isAdminLevel, function (req, res, next) {
    var condition = {
        _id: req.params.id
    };
    console.log(req.body);
    var data = {role:req.body.role};
    user.update(condition, data, function (err, user) {
        if (!err) {
            res.json(user);
        } else {
            res.status(400).json(validation.handleMongooseError(err));
        }
    });
});

/* delete exiting user*/
router.delete('/:id',auth_helpers.isAdminLevel, validation.deleteYourself, function (req, res, next) {
    var condition = {
        _id: req.params.id
    };
    user.delete(condition, function (err, result) {
       if(!err)
           res.json(result);
       else
           res.status(400).json(validation.handleMongooseError(err));
    });
});


module.exports = router;
