var express = require('express');
var router = express.Router();

var user = require('../model/user');
var auth_helper = require('../helpers/authentication');

/* GET home page. */
router.get('/', function(req, res, next) {
    if (req.session.user) {
        var condition ={_id:req.session.user._id};
        user.findOne(condition,function(err, result){
            if(!err)
                res.render('index', { user: req.session.user});
            else
                res.render('index', { user: null});
        })

    } else {
        res.render('login', { title: 'login', error:null });
    }
});

/* GET users page */
router.get('/users',auth_helper.isAdminView, function(req, res, next) {
    user.listAll(function(err, users){
        console.log(users);
        if(!err) {
            res.render('users', {users: users, user: req.session.user});
        }else{
            res.render('error', {
                message: err.message,
                error: {}
            });
        }
    });
});

/* GET profile page */
router.get('/profile', function(req, res, next) {
    var condition = {
        _id:req.session.user._id
    };
    user.findOne(condition, function(err, user){
        res.render('profile', {user: user, err: err});
    });
});

module.exports = router;
