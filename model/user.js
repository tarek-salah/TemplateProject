/**
 * Created by tarek on 9/8/15.
 */

var user = require('./mongoose').User;
var record = require('./mongoose').Record;

exports.insert = function (data,callback){
    var user_entry = new user(data);
    user_entry.save(callback);
};

exports.findOne = function (condition,callback) {
    user.findOne(condition , callback);
};


exports.listAll = function(callback){
    user.find({},'-salt -hash', callback);
};

exports.count = function(condition, callback){
    user.count(condition, callback);
};

exports.update = function(condition, data,callback){
    user.findOneAndUpdate(
        condition , { $set: data },{new:true}, callback
    );
};

exports.delete = function(condition,callback){
    user.remove(condition, callback);
};


exports.findRecords = function (condition,callback){
    user.findOne(condition ,'records', callback);
};


exports.insertRecord = function (condition, data, callback){
    var record_entry = new record(data);
    user.findOneAndUpdate(
        condition , { $push: {records : record_entry} },{new:true}, callback
    );
};

exports.updateRecord = function(condition, data, callback){
    var record_entry = new record(data);
    user.findOneAndUpdate(
        condition , { $set: {'records.$' : record_entry} },{new:true}, function(err, result){
            if(err){
                callback(err, null);
            }else{
                callback(null, record_entry)
            }
        }
    );
};

exports.deleteRecord = function(user_condition, record_condition, callback){
    user.update(user_condition, {
        '$pull': {
            'records':record_condition
        }
    }, callback);
};
