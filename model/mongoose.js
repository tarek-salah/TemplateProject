var mongoose = require('mongoose');

mongoose.connect(config.mongodb);


var UserSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true  },
    password: String,
    salt: String,
    hash: String,
    role:{ type: String, required: true },
    first_name:{ type: String, required: true },
    last_name:{ type: String, required: true },
});

exports.User = mongoose.model('users', UserSchema);